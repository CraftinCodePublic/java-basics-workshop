package com.craftincode.quiz.zadanie_4;

import java.util.ArrayList;
import java.util.List;

public class Quiz {
    public static void main(String[] args) {
        System.out.println(">>>>>>>> Craftin' Code QUIZ <<<<<<<<");

        List<String> pytania = new ArrayList<>();
        pytania.add("Jak ma na imię założyciel Microsoftu?");
        pytania.add("Jak nazywa się miasto, w którym znajduje się Panorama Racławicka");
        pytania.add("Więcej niż jedno zwierzę?");

        int licznikPytan = 0;
        int rozmiarListy = pytania.size();

        while (licznikPytan < rozmiarListy){
            System.out.println("Pytanie: " + pytania.get(licznikPytan));

            licznikPytan++;
        }
    }
}
