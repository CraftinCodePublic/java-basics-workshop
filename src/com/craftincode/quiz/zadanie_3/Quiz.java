package com.craftincode.quiz.zadanie_3;

import java.util.ArrayList;
import java.util.List;

public class Quiz {
    public static void main(String[] args) {
        System.out.println(">>>>>>>> Craftin' Code QUIZ <<<<<<<<");

        List<String> pytania = new ArrayList<>();
        pytania.add("Jak ma na imię założyciel Microsoftu?");
        pytania.add("Jak nazywa się miasto, w którym znajduje się Panorama Racławicka");
        pytania.add("Więcej niż jedno zwierzę?");

        String pytanie1 = pytania.get(0);
        String pytanie2 = pytania.get(1);
        String pytanie3 = pytania.get(0);

        System.out.println("Pytanie pierwsze: " + pytanie1);
        System.out.println("Pytanie drugie: " + pytanie2);
        System.out.println("Pytanie trzecie: " + pytanie3);
    }
}
