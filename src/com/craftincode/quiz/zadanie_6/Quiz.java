package com.craftincode.quiz.zadanie_6;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Quiz {
    public static void main(String[] args) {
        System.out.println(">>>>>>>> Craftin' Code QUIZ <<<<<<<<");

        List<String> pytania = new ArrayList<>();
        pytania.add("Jak ma na imię założyciel Microsoftu?");
        pytania.add("Jak nazywa się miasto, w którym znajduje się Panorama Racławicka");
        pytania.add("Więcej niż jedno zwierzę?");

        List<String> odpowiedzi = new ArrayList<>();
        odpowiedzi.add("Bill");
        odpowiedzi.add("Wrocław");
        odpowiedzi.add("Lama");

        int licznikPytan = 0;
        int rozmiarListy = pytania.size();

        while (licznikPytan < rozmiarListy){
            String aktualnePytanie = pytania.get(licznikPytan);
            String aktualnaOdpowiedz = odpowiedzi.get(licznikPytan);

            System.out.println("Pytanie: " + aktualnePytanie);
            System.out.println("Odpowiedź: " + aktualnaOdpowiedz);

            String odpowiedzUzytkownika = new Scanner(System.in).nextLine();

            System.out.println("Twoja odpowiedź: " + odpowiedzUzytkownika);

            licznikPytan++;
        }
    }
}
