package com.craftincode.quiz.zadanie_9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Quiz {
    public static void main(String[] args) {
        System.out.println(">>>>>>>> Craftin' Code QUIZ <<<<<<<<");

        List<String> pytania = new ArrayList<>();
        pytania.add("Jak ma na imię założyciel Microsoftu?");
        pytania.add("Jak nazywa się miasto, w którym znajduje się Panorama Racławicka");
        pytania.add("Więcej niż jedno zwierzę?");

        List<String> odpowiedzi = new ArrayList<>();
        odpowiedzi.add("Bill");
        odpowiedzi.add("Wrocław");
        odpowiedzi.add("Lama");

        int indeksPytania = 0;
        int licznikPunktow = 0;
        int rozmiarListy = pytania.size();

        while (indeksPytania < rozmiarListy){
            String aktualnePytanie = pytania.get(indeksPytania);
            String aktualnaOdpowiedz = odpowiedzi.get(indeksPytania);

            System.out.println("===== Pytanie nr " + indeksPytania + " =====");
            System.out.println(aktualnePytanie);

            String odpowiedzUzytkownika = new Scanner(System.in).nextLine();

            boolean czyOdpowiedzPoprawna = odpowiedzUzytkownika.equalsIgnoreCase(aktualnaOdpowiedz);

            if(czyOdpowiedzPoprawna){
                System.out.println("Brawo, poprawna odpowiedź!");
                licznikPunktow++;
            } else {
                System.out.println("Błędna odpowiedź :( Poprawna to: " + aktualnaOdpowiedz);
            }
            System.out.println("--- Twój wynik to: " + licznikPunktow + " ---");

            indeksPytania++;
        }

        System.out.println(">>>>>>>> KONIEC QUIZU <<<<<<<<");
    }
}
