# Warsztaty z podstaw Javy - QUIZ

> Tutorial stworzony przez szkołę programowania [Craftin' Code](https://www.craftincode.com)

Zapraszamy również do polubienia naszych fanpage'y na Facebooku -  [Craftin' Code](https://www.facebook.com/craftincode/) i [Programuj.pl](https://www.facebook.com/programujpl/) gdzie na bieżąco publikujemy informacje na temat naszych kursów, wydarzeń oraz przydatne informacje wszystkim, którzy uczą się programowania.

# Jak korzystać z projektu.

Projekt został podzielony na zadania - do każdego z zadań jest osobny pakiet ('folder') reprezentujący stan po rozwiązaniu go. Dodatkowo możesz ściągnąć cały projekt i otworzyć go u siebie -  chmurka obok przycisku `Web IDE` -> `Download .zip`. Oprócz tego do każdego zadania stworzyliśmy równieź wersję z komentarzami objaśniającymi kod - możesz sie na nią przełączyć klikając na listę rozwijaną `master` i wybierając `comments`

# Zadania
1. Stwórz nową klasę `Quiz`, utwórz w niej metodę `main` oraz dodaj polecenie wypisujące w konsoli napis `>>>>>>>> Craftin' Code QUIZ <<<<<<<<`
2. Stwórz listę Stringów o nazwie `pytania` i dodaj do niej 3 przykładowe pytania
3. Stwórz 3 zmienne typu String o nazwach `pytanie1, pytanie2, pytanie3` i przypisz im kolejno elementy z listy `pytania` (`pytania.get(0), pytania.get(1), pytania.get(2)`). Wypisz zawartość tych zmiennych.
4. Stwórz zmienną `int licznikPytan = 0` oraz `int rozmiarListy = pytania.size()`. Stwórz pętlę `while` wypisującą wszystkie pytania - powinna wykonywać się **dopóki `licznikPytan < rozmiarListy`** i w każdym kroku zwiększać `licznikPytan` o 1. Element z listy możesz wyciągnąć za pomocą metody `get(INDEKS)`
5. Dodaj analogiczną listę `odpowiedzi` z odpowiedziami do zdefiniowanych pytań. Wewnątrz pętli stwórz zmienne `aktualnePytanie` oraz `aktulnaOpowiedz` do przechowywania odpowiednio pytania i odpowiedzi o indeksie `licznikPytan`. Użyj ich aby wypisać wszystkie pytania i odpowiedzi.
6. Dodaj wewnątrz pętli wczytywanie odpowiedzi od użytkownika (`new Scanner(System.in).nextString()`). Wypisz wczytany tekst. Usuń wypisanie poprawnej odpowiedzi.
7. Dodaj sprawdzanie poprawności odpowiedzi (metoda `equalsIgnoreCase(...)`).
    - jeśli odpowiedź poprawna - wypisuje tekst `Brawo, poprawna odpowiedź!`
    - jeśli odpowiedź niepoprawna - wypisuje tekst `Błędna odpowiedź :( Poprawna to: ODPOWIEDZ`
8. Dodaj przed pętlą zmienną `licznikPunktów` typu `int`. Po każdej poprawnej odpowiedzi zwiększaj licznik o **1**. Po każdej odpowiedzi wypisuj stan licznika - `--- Twój wynik to: XXX ---`
9. Dodaj wypisywanie pożegnania na końcu programu - `>>>>>>>> KONIEC QUIZU <<<<<<<<`. Dodaj wypisywanie informacji o aktualnym pytaniu - `===== Pytanie nr XXX =====`

# Zadania opcjonalne
1. Dodaj losowanie pytań zamiast odczytywanie ich po kolei. Liczbę możesz wylosować z użyciem klasy `Random` - zobacz przykłady - [Losowanie liczb w Javie](https://programuj.pl/snippet/bb67cbe9-bf4f-4702-be1b-9916a50bd7c9)
2. Dodaj do mechanizmu losowania usuwanie pytania (i odpowiedzi) z listy, jeśli użytkownik odpowiedział poprawnie. Element z listy możesz usunąć metodą `remove(...)`. Zobacz przykład - [Podstawowe operacje na listach](https://programuj.pl/snippet/6b6641cb-267a-47da-b7ad-0858681209c2)

# Przydatne linki
1. [Instalacja i konfiguracja środowiska](https://programuj.pl/blog/java-instalacja-konfiguracja)
2. [Zmienne](https://programuj.pl/blog/java-zmienne)
3. [Typy prymitywne](https://programuj.pl/blog/java-typy-prymitywne)
4. [Instrukcje warunkowe](https://programuj.pl/blog/java-instrukcje-warunkowe-if-switch)
5. [Pętle](https://programuj.pl/blog/java-petle-for-while-foreach)
6. [Podstawowe operacje na listach](https://programuj.pl/snippet/6b6641cb-267a-47da-b7ad-0858681209c2)